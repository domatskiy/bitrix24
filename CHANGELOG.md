### Changelog

##### 2.2.0
 - lead list: add start option
 - deal list
 - deal fields
 - status list
 - status entity type list
 - contact list
 
##### 2.1.1
 - fix bug: webHookUrl
  
##### 2.1.0  
 - исключение, при ответе, отличном от 200 (ResponseException)
 - обновление версий зависимостей
 - возможность задать таймаут
 - отключение верификации сертификата
 - функция обновления лида
 
