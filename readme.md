# Bitrix24

* rest api for Bitrix24

## install 

```
composer require domatskiy/bitrix24
```

##usage

```php
# create WebHook
$App = new WebHook(<host>, <userId>, <hookCode>);

@mkdir(__DIR__.'/log');
$logger = new Logger('webhook');
$logger->pushHandler(new StreamHandler(__DIR__.'/log/app-webhook-'.date('Y-m-d').'.log',Logger::DEBUG));
$App->setLogger($logger);

// create rest
$RestLead = new \Domatskiy\Bitrix24\Rest\CRM\Lead($App);

// send request
$result = $RestLead->add(<lead>);
```
  
###create lead
```php 
$lead = new Bitrix24\Lead('Request: ', Bitrix24\Lead::SOURCE_WEB, Bitrix24\Lead::STATUS_NEW, Bitrix24\Lead::CURRENCY_RUB);

# add fields to lead
$lead->addField(\Domatskiy\Bitrix24\Lead::FIELD_NAME, $user_name);
$lead->addField(\Domatskiy\Bitrix24\Lead::FIELD_PHONE_MOBILE, $phone);
$lead->addField(\Domatskiy\Bitrix24\Lead::FIELD_EMAIL_HOME, $email);

# adding additional fields
$lead->addFieldExt('UF_XXXXXXX', '');

# adding file
$file = Lead\File($absolute_path);
$lead->addFile('UF_XXXXXXX', $file);

# adding files
$files = Lead\Files();
$files->addFile($url);
$lead->addFiles('UF_XXXXXXX', $files);
```
