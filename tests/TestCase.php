<?php

namespace Tests;

use Matomo\Ini\IniReader;
use PHPUnit\Framework\TestCase  as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected $bxHost;
    protected $bxPort;
    protected $bxLogin;
    protected $bxPassword;
    protected $bxHookCode;
    protected $bxUserId;

    protected $bxFieldFile;
    protected $bxFieldFiles;

    public function setUp()
    {
        parent::setUp();

        $config_file = __DIR__ . '/config.ini';

        if (file_exists($config_file)) {
            $reader = new IniReader();
            $config = $reader->readFile($config_file);
            echo "load config.ini\n";

            if (isset($config['bitrix24'])) {
                $configBx = $config['bitrix24'];

                if (isset($configBx['host']) && $configBx['host']) {
                    putenv('BITRIX24_HOST='.$configBx['host']);
                }

                if (isset($configBx['port']) && $configBx['port']) {
                    putenv('BITRIX24_PORT='.$configBx['port']);
                }

                if (isset($configBx['login']) && $configBx['login']) {
                    putenv('BITRIX24_LOGIN='.$configBx['login']);
                }

                if (isset($configBx['password']) && $configBx['password']) {
                    putenv('BITRIX24_PASSWORD='.$configBx['password']);
                }

                if (isset($configBx['userId']) && $configBx['userId']) {
                    putenv('BITRIX24_USER_ID='.$configBx['userId']);
                }

                if (isset($configBx['hookCode']) && $configBx['hookCode']) {
                    putenv('BITRIX24_WEBHOOK='.$configBx['hookCode']);
                }
            }

            if (isset($config['fields'])) {
                $configFields = (array)$config['fields'];
                if (isset($configFields['file']) && $configFields['file']) {
                    putenv('BITRIX24_FIELD_FILE='.$configFields['file']);
                }

                if (isset($configFields['files']) && $configFields['files']) {
                    putenv('BITRIX24_FIELD_FILES='.$configFields['files']);
                }
            }
        } else {
            echo "config.ini not found\n";
        }

        $this->bxHost = getenv('BITRIX24_HOST');
        $this->bxUserId = (string)getenv('BITRIX24_USER_ID');
        $this->bxLogin = (string)getenv('BITRIX24_LOGIN');
        $this->bxPassword = (string)getenv('BITRIX24_PASSWORD');
        $this->bxHookCode = (string)getenv('BITRIX24_WEBHOOK');

        $this->bxFieldFile = getenv('BITRIX24_FIELD_FILE');
        $this->bxFieldFiles = getenv('BITRIX24_FIELD_FILES');
    }
}
