<?php
namespace Tests\Unit\Entity;

use Domatskiy\Bitrix24\Entity\Lead;
use Exception;
use PHPUnit\Framework\TestCase;

class FileFieldTest extends TestCase
{
    protected $file_one_path;

    public function setUp()
    {
        parent::setUp();

        $this->file_one_path = __DIR__.'/files/one.txt';
    }

    /**
     * @throws Exception
     */
    public function testFileField()
    {
        $file = new Lead\File($this->file_one_path);
        $this->assertTrue($file->getPath() == $this->file_one_path, 'err file path');
        $this->assertTrue($this->file_one_path === $file->getPath(), 'not correct path');

        $this->expectException(Exception::class);
        new Lead\File(__DIR__.'/one__none.txt');
    }

    /**
     * @throws Exception
     */
    public function testFilesField()
    {
        $files = new Lead\Files();
        $url = 'http://test.ru/pndd.png';
        $files->addFile($url);

        $this->expectException(Exception::class);
        $files->addFile('test/pndd.png');
    }
}
