<?php
namespace Tests\Unit\Entity;

use Domatskiy\Bitrix24\Entity\Lead;
use PHPUnit\Framework\TestCase;

class LeadTest extends TestCase
{
    protected $file_one_path;

    public function setUp()
    {
        parent::setUp();

        $this->file_one_path = __DIR__.'/files/one.txt';
    }

    public function testCreateLead()
    {
        $lead = new Lead('TEST');

        $lead->setField('TITLE', 'test_name');
        $lead->setFieldExt('EXT', 'EXT_VAL');

        $file = new Lead\File($this->file_one_path);
        $lead->setFieldExt('TEST_FILE', $file);
        $lead->addFile('TEST_FILE', $file);

        $file_multi = new Lead\Files([
            'http://test.ru/file1.txt',
            'http://test.ru/file2.txt',
        ]);

        $lead->setFieldExt('TEST_FILES', $file_multi);
        $lead->addFiles('TEST_FILES', $file_multi);

        $fields = $lead->getFields();
        $this->assertTrue(isset($fields['TITLE']));
        $this->assertTrue(isset($fields['EXT']));

        $fileFields = $lead->getFileFields();
        $this->assertTrue(isset($fileFields['TEST_FILE']));
        $this->assertTrue(isset($fileFields['TEST_FILES']));
    }

}
