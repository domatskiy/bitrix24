<?php
namespace Tests\Unit;

use Domatskiy\Bitrix24\Importer\Connection;
use PHPUnit\Framework\TestCase;

class ConnectionTest extends TestCase
{
    public function testCreateConnection()
    {
        $connection = new Connection('8.8.8.8', null, 'test', 'test');
        $this->assertTrue($connection->getHost() == '8.8.8.8', 'error host');
        $this->assertTrue($connection->getPort() === null, 'error port: '.$connection->getPort());
    }
}
