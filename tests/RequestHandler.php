<?php

namespace Tests;

use Exception;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

trait RequestHandler
{
    /**
     * @param $path
     * @param int $status
     *
     * @return HandlerStack
     * @throws Exception
     */
    protected function getHandler(string $path, $status = 200):HandlerStack
    {
        $response = $this->getResponseFromFile($path, $status);

        // Create a mock and queue two responses.
        $mock = new MockHandler([
            $response
        ]);

        return HandlerStack::create($mock);
    }

    /**
     * @param string $path
     * @param int $status
     * @return Response
     * @throws Exception
     */
    protected function getResponseFromFile(string $path, int $status): Response
    {
        if ($status < 1) {
            throw new Exception('not correct status');
        }

        $stream = file_get_contents($path);
        return new Response($status, [], $stream);
    }

    /**
     * @param string $string
     * @param int $status
     * @return Response
     * @throws Exception
     */
    protected function getResponseFromString(string $string, int $status = 200): Response
    {
        if ($status < 1) {
            throw new Exception('not correct status');
        }

        $stream = fopen('data://text/plain;base64,' . base64_encode($string), 'r');
        $stream = stream_get_contents($stream);

        return new Response($status, [], $stream);
    }
}
