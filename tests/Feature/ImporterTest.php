<?php
namespace Tests\Feature;

use Domatskiy\Bitrix24\Importer\Connection;
use Domatskiy\Bitrix24\Importer\Entity\Lead;
use Domatskiy\Bitrix24\Importer\LeadImporter;
use Domatskiy\Bitrix24\Importer\Result;

use Tests\TestCase;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ImporterTest extends TestCase
{
    protected $file_path1;
    protected $file_path2;

    public function setUp()
    {
        parent::setUp();

        $this->file_path1 = __DIR__.'/files/one.txt';
        $this->file_path2 = __DIR__.'/files/two.txt';
    }

    public function testSubmitMock()
    {
        $connection = new Connection('localhost', 80, 'test', 'test');

        /**
         * @var $bitrix24Stub \Domatskiy\Bitrix24\Importer\LeadImporter
         */
        $bitrix24Stub = $this->getMockBuilder('\Domatskiy\Bitrix24\Importer\LeadImporter')
            ->setConstructorArgs([$connection])
            ->getMock();

        echo PHP_EOL.' test with stub !!!'.PHP_EOL;

        // Настройка заглушки.
        $bitrix24Stub->expects($this->any())
            ->method('sendLead')
            ->will($this->returnValue(new Result(777, 'message')));
    }

    /**
     * @throws \Domatskiy\Bitrix24\Exception\ArgumentException
     * @throws \Domatskiy\Bitrix24\Exception\AuthException
     * @deprecated
     */
    public function __testSubmitReal()
    {
        $this->assertTrue(!!$this->bxHost, 'not defined: bxHost');
        $this->assertTrue(!!$this->bxUserId, 'not defined: bxUserId');
        $this->assertTrue(!!$this->bxHookCode, 'not defined: bxHookCode');

        $connection = new Connection($this->bxHost, $this->bxPort, $this->bxLogin, $this->bxPassword);
        $bitrix24 = new LeadImporter($connection);

        $logger = new Logger('bitrix24_lead_send');
        $date = date('Y-m-d');

        @mkdir(__DIR__.'/log');

        $logger->pushHandler(new StreamHandler(
            __DIR__.'/log/bitrix24-'.$date.'.log',
            Logger::DEBUG
        ));

        $bitrix24->setLogger($logger);

        $leadName = 'TEST_SEND '.date('Y-m-d H:i:s');
        echo $leadName."\n";

        $lead = new Lead($leadName, Lead::SOURCE_WEB, Lead::STATUS_IN_PROCESS);

        $lead->addField(Lead::FIELD_NAME, 'user_name');
        $lead->addField(Lead::FIELD_PHONE, '+79111111111');
        $lead->addField(Lead::FIELD_EMAIL_HOME, 'test@test.ru');
        $lead->addField(Lead::FIELD_UTM_SOURCE, 'test');
        $lead->addField(Lead::FIELD_UTM_CAMPAIGN, 'comp1');

        // create file
        $file_one = new Lead\File($this->file_path1);

        // create multi file
        $file_multi = new Lead\Files();
        $file_multi->addFile('https://dummyimage.com/200/c51d34/file1.png');
        $file_multi->addFile('https://dummyimage.com/200/e52b50/file2.png');

        // FILE
        if ($this->bxFieldFile) {
            $lead->addFile($this->bxFieldFile, $file_one);
        }

        // FILES
        if ($this->bxFieldFiles) {
            $lead->addFieldExt($this->bxFieldFiles, $file_multi);
        }

        // submit
        $rs = $bitrix24->sendLead($lead);
        $this->assertInstanceOf(Result::class, $rs, 'not correct send result');

        echo PHP_EOL.'======================'.PHP_EOL;
        var_dump($rs->getID(), $rs->getMessage());
    }
}
