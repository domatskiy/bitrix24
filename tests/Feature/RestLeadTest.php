<?php
namespace Tests\Feature;

use Domatskiy\Bitrix24\ApplicationInterface;
use Domatskiy\Bitrix24\Entity\Lead;

use Domatskiy\Bitrix24\Exception\ResponseException;
use Domatskiy\Bitrix24\Response\CRM\Lead\ListResponse;
use Domatskiy\Bitrix24\Response\Time;
use Domatskiy\Bitrix24\Rest\Response;
use Domatskiy\Bitrix24\WebHook;
use Tests\RequestHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Tests\TestCase;

class RestLeadTest extends TestCase
{
    use RequestHandler;

    protected $file_path1;
    protected $file_path2;

    public function setUp()
    {
        parent::setUp();

        $this->file_path1 = __DIR__.'/files/one.txt';
        $this->file_path2 = __DIR__.'/files/two.txt';
    }

    protected function getTestWebHook(string $responseFile, int $status = 200):ApplicationInterface
    {
        $handler = $this->getHandler($responseFile, $status);

        $WH = new WebHook('test.ru', 1, 'xyz');
        $WH->setTimout(10);
        $WH->setVerify(true);
        $WH->setHandler($handler);

        return $WH;
    }

    public function testRestLeadGetList()
    {
        $WH = $this->getTestWebHook(__DIR__.'/response/lead_list.json');
        $Leads = new \Domatskiy\Bitrix24\Rest\CRM\Lead($WH);
        $result = $Leads->list([], [], []);

        $this->assertInstanceOf(ListResponse::class, $result);
        $this->assertTrue(is_array($result->getResult()));
        $this->assertInstanceOf(Time::class, $result->getTime());

        $this->assertCount(1, $result->getResult());

        foreach ($result->getResult() as $item) {
            $this->assertEquals('Входящий звонок от 89867777011', $item->TITLE);
            $this->assertEquals('Тестович', $item->NAME);
            $this->assertEquals('777099', $item->CONTACT_ID);

            $this->assertCount(1, $item->PHONE);
            foreach ($item->PHONE as $phone) {
                $this->assertEquals('1234440', $phone->ID);
                $this->assertEquals('WORK', $phone->VALUE_TYPE);
                $this->assertEquals('89867777011', $phone->VALUE);
                $this->assertEquals('PHONE', $phone->TYPE_ID);
            }
        }
    }

    public function testRestLeadAdd()
    {
        $WH = $this->getTestWebHook(__DIR__.'/response/lead_add.json');
        $Leads = new \Domatskiy\Bitrix24\Rest\CRM\Lead($WH);
        $lead = new Lead('test');
        $result = $Leads->add($lead);

        $this->assertEquals(777099, $result->getResult());
        $this->assertEquals(200, $result->getStatus());
        $this->assertInstanceOf(Time::class, $result->getTime());
    }

    public function testRestLeadUpdate()
    {
        $WH = $this->getTestWebHook(__DIR__.'/response/lead_update.json');
        $Leads = new \Domatskiy\Bitrix24\Rest\CRM\Lead($WH);
        $result = $Leads->update(777, [
            'TITLE' => 'new',
        ]);

        $this->assertTrue($result->getResult());
        $this->assertEquals(200, $result->getStatus());
        $this->assertInstanceOf(Time::class, $result->getTime());
    }

    public function testRestLeadUpdateWithError()
    {
        $WH = $this->getTestWebHook(__DIR__.'/response/lead_update_error.json', 401);
        $Leads = new \Domatskiy\Bitrix24\Rest\CRM\Lead($WH);

        $this->expectException(ResponseException::class);
        $this->expectExceptionCode(401);
        $Leads->update(999, []);

        /**
         * @var $ex ResponseException
         */
        $ex = $this->getExpectedException();
        var_dump($ex);
    }

    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @deprecated
     */
    public function __testSubmitReal()
    {
        $this->assertTrue(!!$this->bxHost, 'not defined: bxHost');
        $this->assertTrue(!!$this->bxUserId, 'not defined: bxUserId');
        $this->assertTrue(!!$this->bxHookCode, 'not defined: bxHookCode');

        $App = new WebHook($this->bxHost, $this->bxUserId, $this->bxHookCode);

        @mkdir(__DIR__.'/log');
        $logger = new Logger('webhook');
        $logger->pushHandler(new StreamHandler(__DIR__.'/log/app-webhook-'.date('Y-m-d').'.log',Logger::DEBUG));
        $App->setLogger($logger);

        $Rest = new \Domatskiy\Bitrix24\Rest\CRM\Lead($App);

        $leadName = 'lead name: LEAD WEB_HOOK '.date('Y-m-d H:i:s');
        echo $leadName."\n";

        $lead = new Lead($leadName, Lead::SOURCE_WEB, Lead::STATUS_IN_PROCESS);

        $lead->setField(Lead::FIELD_NAME, 'user_name');
        $lead->setField(Lead::FIELD_PHONE, [
            ['VALUE_TYPE' => 'MOBILE', 'VALUE' => '+79111111111',]
        ]);

        $lead->setField(Lead::FIELD_EMAIL, [
            ['VALUE_TYPE' => 'HOME', 'VALUE' => 'test@test.ru',]
        ]);

        $lead->setField(Lead::FIELD_UTM_SOURCE, 'test');
        $lead->setField(Lead::FIELD_UTM_CAMPAIGN, 'comp1');

        // FILE
        if ($this->bxFieldFile) {
            // create file
            $file_one = new Lead\File($this->file_path1);

            $lead->setFieldExt($this->bxFieldFile, $file_one);
        }

        // FILES
        if ($this->bxFieldFiles) {
            // create multi file
            $files = new Lead\Files();
            $files->addFile('https://dummyimage.com/200/c51d34/file1.png');
            $files->addFile('https://dummyimage.com/200/e52b50/file2.png');
            $files->addFile($this->file_path1);
            $files->addFile($this->file_path1);

            $lead->setFieldExt($this->bxFieldFiles, $files);
        }

        echo "=========================\n";
        echo "lead object:\n";
        var_dump($lead);

        // submit
        $rs = $Rest->add($lead);
        $this->assertInstanceOf(Response::class, $rs, 'not correct send result');
        $this->assertTrue($rs->getResult() > 0);

        echo "=========================\n";
        echo "lead add result:\n";
        var_dump($rs);

        # echo "=========================\n";
        # echo "get lead:\n";
        # $Rest = new \Domatskiy\Bitrix24\Rest\CRM\Lead($App);
        # $rs = $Rest->get($rs->getResult());

        # echo "=========================\n";
        # echo "get lead result:\n";
        # var_dump($rs);

        echo PHP_EOL.'======================'.PHP_EOL;
    }
}
