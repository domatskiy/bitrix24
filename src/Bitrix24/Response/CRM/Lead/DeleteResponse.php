<?php


namespace Domatskiy\Bitrix24\Response\CRM\Lead;

use Domatskiy\Bitrix24\Rest\Response;

/**
 * Class DeleteResponse
 * @package Domatskiy\Bitrix24\Response\CRM\Lead
 */
class DeleteResponse extends Response
{
    //
}
