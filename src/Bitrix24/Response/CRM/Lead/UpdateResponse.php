<?php


namespace Domatskiy\Bitrix24\Response\CRM\Lead;

use Domatskiy\Bitrix24\Rest\Response;

class UpdateResponse extends Response
{
    /**
     * @var bool|null
     */
    protected $result;

    public function getResult():?bool
    {
        return $this->result;
    }
}
