<?php

namespace Domatskiy\Bitrix24\Response\CRM\Lead;

use Domatskiy\Bitrix24\Rest\Response;

/**
 * Class GetResponse
 * @package Domatskiy\Bitrix24\Response\CRM\Lead
 * "result":{
 *      "ID":"248",
 *      "TITLE":"TEST_SEND 2020-01-30 08:47:31",
 *      "HONORIFIC":null,
 *      "NAME":"user_name",
 *      "SECOND_NAME":"",
 *      "LAST_NAME":"",
 *      "COMPANY_TITLE":"",
 *      "COMPANY_ID":null,
 *      "CONTACT_ID":null,
 *      "IS_RETURN_CUSTOMER":"N",
 *      "BIRTHDATE":"",
 *      "SOURCE_ID":"WEB",
 *      "SOURCE_DESCRIPTION":"",
 *      "STATUS_ID":"IN_PROCESS",
 *      "STATUS_DESCRIPTION":"",
 *      "POST":"","COMMENTS":"",
 *      "CURRENCY_ID":"RUB",
 *      "OPPORTUNITY":"0.00",
 *      "HAS_PHONE":"Y",
 *      "HAS_EMAIL":"Y","HAS_IMOL":"N","ASSIGNED_BY_ID":"1","CREATED_BY_ID":"1","MODIFY_BY_ID":"1",
 *      "DATE_CREATE":"2020-01-30T11:47:33+03:00","DATE_MODIFY":"2020-01-30T11:47:33+03:00","DATE_CLOSED":"",
 *      "STATUS_SEMANTIC_ID":"P","OPENED":"Y","ORIGINATOR_ID":null,"ORIGIN_ID":null,"ADDRESS":null,"ADDRESS_2":null,
 *      "ADDRESS_CITY":null,"ADDRESS_POSTAL_CODE":null,"ADDRESS_REGION":null,"ADDRESS_PROVINCE":null,
 *      "ADDRESS_COUNTRY":null,"ADDRESS_COUNTRY_CODE":null,
 *      "UTM_SOURCE":null,"UTM_MEDIUM":null,"UTM_CAMPAIGN":null,"UTM_CONTENT":null,"UTM_TERM":null
 * }
 */
class GetResponse extends Response
{
    /**
     * @var array|null|object|\stdClass
     */
    protected $result;

    public function getResult()
    {
        return $this->result;
    }
}
