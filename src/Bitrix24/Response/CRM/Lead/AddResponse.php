<?php

namespace Domatskiy\Bitrix24\Response\CRM\Lead;

use Domatskiy\Bitrix24\Rest\Response;

class AddResponse extends Response
{
    /**
     * @var int|null
     */
    protected $result;

    /**
     * @return int|null
     */
    public function getResult():?int
    {
        return $this->result;
    }
}
