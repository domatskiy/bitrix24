<?php

namespace Domatskiy\Bitrix24\Response;

class Time
{
    /**
     * @var float|null
     */
    protected $start;
    /**
     * @var float|null
     */
    protected $finish;

    /**
     * @var float|null
     */
    protected $duration;

    /**
     * @var float|null
     */
    protected $processing;

    /**
     * @var \DateTime|string|null
     */
    protected $date_start;

    /**
     * @var \DateTime|string|null
     */
    protected $date_finish;

    /**
     * @return float|null
     */
    public function getStart(): ?float
    {
        return $this->start;
    }

    /**
     * @param float|null $start
     */
    public function setStart(?float $start): void
    {
        $this->start = $start;
    }

    /**
     * @return float|null
     */
    public function getFinish(): ?float
    {
        return $this->finish;
    }

    /**
     * @param float|null $finish
     */
    public function setFinish(?float $finish): void
    {
        $this->finish = $finish;
    }

    /**
     * @return float|null
     */
    public function getDuration(): ?float
    {
        return $this->duration;
    }

    /**
     * @param float|null $duration
     */
    public function setDuration(?float $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return float|null
     */
    public function getProcessing(): ?float
    {
        return $this->processing;
    }

    /**
     * @param float|null $processing
     */
    public function setProcessing(?float $processing): void
    {
        $this->processing = $processing;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param \DateTime|string|null $date_start
     */
    public function setDateStart($date_start): void
    {
        $this->date_start = $date_start;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getDateFinish()
    {
        return $this->date_finish;
    }

    /**
     * @param \DateTime|string|null $date_finish
     */
    public function setDateFinish($date_finish): void
    {
        $this->date_finish = $date_finish;
    }
}
