<?php

namespace Domatskiy\Bitrix24;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class WebHook implements ApplicationInterface
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var string
     */
    protected $hookCode;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HandlerStack
     */
    protected $handler;

    /**
     * @var int
     */
    protected $timout = 30;

    protected $verify = false;

    public function __construct(string $host, int $userId, string $hookCode)
    {
        $this->host = $host;
        $this->userId = $userId;
        $this->hookCode = $hookCode;

        $this->logger = new NullLogger();
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger):void
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function setHandler(HandlerStack $handler):void
    {
        $this->handler = $handler;
    }

    public function setTimout(int $timout): void
    {
        $this->timout = $timout;
    }

    /**
     * @param bool $verify
     */
    public function setVerify(bool $verify): void
    {
        $this->verify = $verify;
    }

    /**
     * @param string $method
     * @param array $data
     * @return ResponseInterface
     */
    public function request(string $method, array $data):ResponseInterface
    {
        $protocol = strpos($this->host, '://') !== false ? '' : 'https://';
        $url = sprintf('%s%s/rest/%s/%s/%s', $protocol, $this->host, $this->userId, $this->hookCode, $method);

        $this->logger->debug('call method '.$method, [
            'url' => $url,
            'data' => $data,
        ]);

        $client = $this->getClient();

        return $client->post($url, [
            'form_params' => $data
        ]);
    }

    protected function getClient(array $options = []):Client
    {
        if ($this->handler) {
            $options['handler'] = $this->handler;
        }

        if ($this->timout) {
            $options[RequestOptions::TIMEOUT] = $this->timout;
        }

        $baseOptions = [
            RequestOptions::HTTP_ERRORS => true,
            RequestOptions::VERIFY => $this->verify,
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
        ];

        return new \GuzzleHttp\Client(array_merge($baseOptions, $options));
    }
}
