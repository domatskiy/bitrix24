<?php

namespace Domatskiy\Bitrix24;

use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface ApplicationInterface
 * @package Domatskiy\Bitrix24
 */
interface ApplicationInterface
{
    /**
     * @param $method
     * @param array $data
     * @return ResponseInterface
     */
    public function request(string $method, array $data):ResponseInterface;

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger):void;

    /**
     * @return LoggerInterface
     */
    public function getLogger():LoggerInterface;
}
