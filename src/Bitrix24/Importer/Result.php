<?php

namespace Domatskiy\Bitrix24\Importer;

class Result
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $message;

    /**
     * @var string|null
     */
    protected $auth;

    public function __construct($id, $message)
    {
        $this->id = $id;
        $this->message = $message;
    }

    /**
     * @return int|null
     */
    public function getID():?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getMessage():?string
    {
        return $this->message;
    }

    /**
     * @param string $auth
     */
    public function setAuth(string $auth): void
    {
        $this->auth = $auth;
    }

    /**
     * @return string
     */
    public function getAuth(): string
    {
        return $this->auth;
    }
}
