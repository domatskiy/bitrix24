<?php

namespace Domatskiy\Bitrix24\Importer;

use Domatskiy\Bitrix24\Importer\Entity\Lead;
use Domatskiy\Bitrix24\Exception\ArgumentException;
use Domatskiy\Bitrix24\Exception\AuthException;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

/**
 * Class Bitrix24
 * @package Domatskiy
 */
class LeadImporter
{
    /**
     * @var $connection ConnectionInterface
     */
    private $connection;

    /**
     * @var bool
     */
    private $debug = false;

    /**
     * @var string
     */
    private $debug_log_path = '';

    private $logger;

    /**
     * LeadImporter constructor.
     * @param \Domatskiy\Bitrix24\Importer\ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param bool $enable
     * @param string $path
     * @throws \Exception
     * @deprecated use setLogger
     */
    public function debug(bool $enable, string $path):void
    {
        $this->debug = $enable;
        $this->debug_log_path = $path;

        if ($this->debug) {
            $this->logger = new Logger('bitrix24_lead_send');

            $date = date('Y-m-d');

            if ($this->debug_log_path) {
                $this->logger->pushHandler(new StreamHandler(
                    $this->debug_log_path.'/bitrix24-'.$date.'.log',
                    Logger::DEBUG
                ));
            }
        }
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(?LoggerInterface $logger):void
    {
        $this->logger = $logger;
    }

    /**
     * @param Lead $lead
     * @return Result
     * @throws ArgumentException
     * @throws AuthException
     * @deprecated use sendLead
     */
    public function send(\Domatskiy\Bitrix24\Lead $lead): Result
    {
        return $this->sendLead($lead);
    }

    /**
     * @param Lead $lead
     * @return Result
     * @throws ArgumentException
     * @throws AuthException
     */
    public function sendLead(Lead $lead): Result
    {
        $postData = [];
        $multipartData = [];

        $postData['LOGIN'] = $this->connection->getLogin();
        $postData['PASSWORD'] = $this->connection->getPassword();
        $postData['method'] = 'lead.add';

        #if (defined('AUTH'))
        #    $postData['AUTH'] = HASH;

        if ($lead->getStatus()) {
            $postData['STATUS_ID'] = $lead->getStatus();
        }

        if ($lead->getSource()) {
            $postData['SOURCE_ID'] = $lead->getSource();
        }

        if ($lead->getCurrency()) {
            $postData['CURRENCY_ID'] = $lead->getCurrency();
        }

        #$postData['PRODUCT_ID'] = 'PRODUCT_1';

        foreach ($lead->getFields() as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $index => $val) {
                    $postData[$key.'['.$index.']'] = $val;
                }
            } else {
                $postData[$key] = $value;
            }
        }

        foreach ($lead->getFileFields() as $key => $value) {
            if ($value instanceof Lead\File) {
                /**
                 * @var $value Lead\File
                 */
                $multipartData[] = [
                    'name'     => $key, # $value->getName(),
                    'contents' => fopen($value->getPath(), 'r')
                ];
            } elseif ($value instanceof Lead\Files) {
                /**
                 * @var $value Lead\Files
                 */
                foreach ($value->getFilesUrl() as $index => $url) {
                    $postData[$key.'['.$index.']'] = $url;
                }
            }
        }

        $url = 'https://'.$this->connection->getHost();

        if ($this->connection->getPort()) {
            $url .= ':'.$this->connection->getPort();
        }

        $url .= '/crm/configs/import/lead.php';

        $client = new \GuzzleHttp\Client([
            'timeout'  => 30.0,
            'verify' => false,
            'headers' => [
                // 'Host' => $this->host,
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
        ]);

        $config = [];

        if (!empty($multipartData)) {
            foreach ($postData as $name => $co) {
                $multipartData[] = [
                    'name' => $name,
                    'contents' => $co
                ];
            }

            $config['multipart'] = $multipartData;
        } else {
            $config['form_params'] = $postData;
        }

        $response = $client->post($url, $config);

        $responseData = null;

        try {
            $contents = $response->getBody()->getContents();
            $status = $response->getStatusCode();

            $contents = str_replace('\'', '"', $contents);
            $responseData = @json_decode($contents);

            $this->logger && $this->logger->debug('$status='.print_r($status, true));
            $this->logger && $this->logger->debug('$responseData='.print_r($responseData, true));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        /**
         * $result=object(stdClass)#694 (4) {
        ["error"]=>
        string(3) "201"
        ["ID"]=>
        string(2) "20"
        ["error_message"]=>
        string(23) "Лид добавлен"
        ["AUTH"]=>
        string(32) "fc1d45860c4a83026b721a8bc938d"
        }
         */

        if ($response->getStatusCode() === 200) { # Лид добавлен
            if (!$responseData) {
                throw new \Exception('not correct response');
            }

            if ($responseData &&
                isset($responseData->error) &&
                isset($responseData->error_message)) {
                switch ((int)$responseData->error) {
                    case 200:
                    case 201:
                        if (!isset($responseData->ID)) {
                            throw new \Exception('not correct response, need ID');
                        }

                        // в bitrix как всегда )
                        // сообщение успешного ответа в error_message
                        $message = isset($responseData->error_message) ? $responseData->error_message : '';
                        $lead_add_result = new Result($responseData->ID, $message);

                        if (isset($responseData->AUTH) && $responseData->AUTH) {
                            $lead_add_result->setAuth($responseData->AUTH);
                        }

                        return $lead_add_result;

                        break;

                    case 400:
                        $this->logger && $this->logger->warning($responseData->error_message);

                        throw new ArgumentException($responseData->error_message, $response->getStatusCode());

                        break;

                    case 403:
                        throw new AuthException($responseData->error_message);
                        break;

                    default:
                        throw new \Exception($responseData->error_message, $responseData->error);
                        break;
                }
            }

            throw new \Exception('not correct response');
        } elseif ($response->getStatusCode() === 400) { # Отсутствуют параметры или параметры не прошли проверку
            $message = 'Отсутствуют параметры';

            $this->logger && $this->logger->warning($message);

            throw new ArgumentException($message, $response->getStatusCode());
        } elseif ($response->getStatusCode() === 403) { # Ошибка авторизации или доступа
            $message = 'Ошибка авторизации';

            $this->logger && $this->logger->warning($message);

            throw new AuthException($message, 403);
        }

        $this->logger && $this->logger->error('Response status: '.$response->getStatusCode());

        throw new \Exception('Необработанный ответ');
    }
}
