<?php

namespace Domatskiy\Bitrix24\Importer;

/**
 * Class Connection
 * @package Domatskiy\Bitrix24
 */
class Connection implements ConnectionInterface
{
    /**
     * @var string
     */
    private $login = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var int|null
     */
    private $port = '';

    /**
     * @var string
     */
    private $host = '';

    public function __construct(string $host, ?int $port = null, string $login = '', string $password = '')
    {
        $this->host = trim($host);
        $this->port = $port;

        $this->login = $login;
        $this->password = $password;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort():?int
    {
        return $this->port;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
