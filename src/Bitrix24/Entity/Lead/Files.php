<?php

namespace Domatskiy\Bitrix24\Entity\Lead;

/**
 * Class Files
 * @package Domatskiy\Bitrix24\Lead
 */
class Files implements \Serializable
{
    /**
     * @var array
     */
    protected $path = [];

    /**
     * @param string $url
     * @throws \Exception
     */
    public function addFile(string $url): void
    {
        if (!filter_var($url, FILTER_VALIDATE_URL) && !file_exists($url)) {
            throw new \Exception('not valid url or file not exits: '.$url);
        }

        $this->path[] = $url;
    }

    /**
     * @return array
     */
    public function getFilesUrl(): array
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'path' => $this->path
        ]);
    }

    /**
     * @param string $data
     */
    public function unserialize($data)
    {
        $d = unserialize($data);

        foreach (get_object_vars($this) as $code) {
            if (array_key_exists($code, $d)) {
                $this->{$code} = $d[$code];
            }
        }
    }
}
