<?php

namespace Domatskiy\Bitrix24\Rest;

use Domatskiy\Bitrix24\Response\Time;

abstract class Response implements ResponseInterface
{
    /**
     * @var int
     */
    protected $status;

    /**
     * @var string|int|array|null
     */
    protected $result;

    /**
     * @var Time|null
     */
    protected $time;

    /**
     * @var string|null
     */
    protected $error;

    /**
     * @var string|null
     */
    protected $error_description;

    /**
     * @return array|int|string|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param array|int|string|null $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }

    /**
     * @return Time|null
     */
    public function getTime():Time
    {
        return $this->time;
    }

    /**
     * @param Time $time
     */
    public function setTime(Time $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     */
    public function setError(?string $error): void
    {
        $this->error = $error;
    }

    /**
     * @return string|null
     */
    public function getErrorDescription(): ?string
    {
        return $this->error_description;
    }

    /**
     * @param string|null $error_description
     */
    public function setErrorDescription(?string $error_description): void
    {
        $this->error_description = $error_description;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
