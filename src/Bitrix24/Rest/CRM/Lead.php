<?php

namespace Domatskiy\Bitrix24\Rest\CRM;

use Domatskiy\Bitrix24\Entity;
use Domatskiy\Bitrix24\Response\CRM\Lead\DeleteResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\FieldsResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\GetResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\UpdateResponse;
use Domatskiy\Bitrix24\Rest\Method;
use Domatskiy\Bitrix24\Response\CRM\Lead\AddResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\ListResponse;

/**
 * Class Lead
 * @package Domatskiy\Bitrix24\CRM
 */
class Lead extends Method
{
    /**
     * Создаёт новый лид
     *
     * @link https://dev.1c-bitrix.ru/rest_help/crm/leads/crm_lead_add.php
     * @link https://dev.1c-bitrix.ru/rest_help/js_library/rest/files.php
     * @param Entity\Lead $lead
     * @param array $params array("REGISTER_SONET_EVENT" => "Y")
     * @return AddResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function add(Entity\Lead $lead, array $params = []):AddResponse
    {
        $this->logger->debug('send lead');

        $postData = $lead->getFields();

        if ($lead->getStatus()) {
            $postData['STATUS_ID'] = $lead->getStatus();
        }

        if ($lead->getSource()) {
            $postData['SOURCE_ID'] = $lead->getSource();
        }

        if ($lead->getCurrency()) {
            $postData['CURRENCY_ID'] = $lead->getCurrency();
        }

        foreach ($lead->getFileFields() as $code => $file) {
            if ($file instanceof Entity\Lead\File) {
                $data = file_get_contents($file->getPath());
                $base64 = base64_encode($data);

                $name = explode('?', basename($file->getPath()));
                $name = $name[0];
                #$type = pathinfo($file->getPath(), PATHINFO_EXTENSION);
                // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $postData[$code] = [
                    'fileData' => [
                        $name,
                        $base64
                    ]
                ];
            } elseif ($file instanceof Entity\Lead\Files) {
                /**
                 * @var $file Entity\Lead\Files
                 */
                $postData[$code] = [];
                foreach ($file->getFilesUrl() as $url) {
                    $name = basename($url);
                    $data = file_get_contents($url);
                    $base64 = base64_encode($data);

                    $postData[$code][] = [
                        'fileData' => [
                            $name,
                            $base64
                        ]
                    ];
                }
            }
        }

        return $this->call('crm.lead.add.json', array(
            'fields' => $postData,
            'params' => $params// array("REGISTER_SONET_EVENT" => "Y")
        ), AddResponse::class);
    }

    /**
     * @param int $id
     * @return \Domatskiy\Bitrix24\Rest\Response|GetResponse
     */
    public function get(int $id)
    {
        $params = [
            'id' => $id,
        ];

        return $this->call('crm.lead.get', $params, GetResponse::class);
    }

    /**
     * Возвращает список лидов по фильтру. Является реализацией списочного метода для лидов.
     * При выборке используйте маски:
     * "*" - для выборки всех полей (без пользовательских и множественных)
     * "UF_*"- для выборки всех пользовательских полей (без множественных)
     * Маски для выборки множественных полей нет. Для выборки множественных
     * полей укажите нужные в списке выбора ("PHONE", "EMAIL" и так далее).
     *
     * @link https://dev.1c-bitrix.ru/rest_help/crm/leads/crm_lead_list.php
     * @param array $order
     * @param array $filter
     * @param array $select
     * @param null $start
     * @return ListResponse
     * @throws \Domatskiy\Bitrix24\Exception\ResponseException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function list(array $order = [], array $filter = [], array $select = [], $start = null):ListResponse
    {
        $params = [
            'order' => $order,
            'filter' => $filter,
            'select' => $select,
        ];

        if ($start) {
            $params['start'] = $start;
        }

        return $this->call('crm.lead.list', $params, ListResponse::class);
    }

    /**
     * @param int $id
     * @param array $fields
     * @return UpdateResponse
     * @throws \Domatskiy\Bitrix24\Exception\ResponseException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function update(int $id, array $fields):UpdateResponse
    {
        $data = [
            'id' => $id,
            'fields' => $fields,
        ];

        return $this->call('crm.lead.update', $data, UpdateResponse::class);
    }

    /**
     * Удаляет лид и все связанные с ним объекты.
     *
     * @link https://dev.1c-bitrix.ru/rest_help/crm/leads/crm_lead_delete.php
     * @param int $id
     * @return DeleteResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function delete(int $id):DeleteResponse
    {
        $params = [
            'id' => $id,
        ];

        return $this->call('crm.lead.delete', $params, DeleteResponse::class);
    }

    /**
     * Возвращает описание полей лида, в том числе пользовательских.
     *
     * @link https://dev.1c-bitrix.ru/rest_help/crm/leads/crm_lead_fields.php
     * @param int $id
     * @return FieldsResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function fields():FieldsResponse
    {
        $params = [];

        return $this->call('crm.lead.fields', $params, FieldsResponse::class);
    }
}
