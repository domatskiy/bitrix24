<?php

namespace Domatskiy\Bitrix24\Rest\CRM;

use Domatskiy\Bitrix24\Response\CRM\Lead\ListResponse;
use Domatskiy\Bitrix24\Rest\Method;

class Status extends Method
{
    public function list(array $select = [], ?array $filter = array(), ?array $order = null, $start = null):ListResponse
    {
        $params = [];

        if (empty($select)) {
            $select = ['ID', 'TITLE'];
        }

        $params['select'] = $select;

        if ($filter) {
            $params['filter'] = $filter;
        }

        if ($start) {
            $params['order'] = $order;
        }

        if ($start) {
            $params['start'] = $start;
        }

        return $this->call('crm.contact.list', $params, ListResponse::class);
    }

    public function getEntityTypes()
    {
        return $this->call('crm.status.entity.types', [], ListResponse::class);
    }
}
