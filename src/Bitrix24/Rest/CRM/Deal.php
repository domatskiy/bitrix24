<?php

namespace Domatskiy\Bitrix24\Rest\CRM;

use Domatskiy\Bitrix24\Entity;
use Domatskiy\Bitrix24\Response\CRM\Lead\DeleteResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\FieldsResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\GetResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\UpdateResponse;
use Domatskiy\Bitrix24\Rest\Method;
use Domatskiy\Bitrix24\Response\CRM\Lead\AddResponse;
use Domatskiy\Bitrix24\Response\CRM\Lead\ListResponse;

/**
 * Class Lead
 * @package Domatskiy\Bitrix24\CRM
 */
class Deal extends Method
{
    /**
     * @param array $select
     * @param array|null $filter
     * @param array|null $order
     * @param null $start
     * @return ListResponse
     * @throws \Domatskiy\Bitrix24\Exception\ResponseException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @see https://dev.1c-bitrix.ru/rest_help/crm/cdeals/crm_deal_list.php
     */
    public function list(array $select = [], ?array $filter = array(), ?array $order = null, $start = null):ListResponse
    {
        $params = [];

        if (empty($select)) {
            $select = ['ID', 'TITLE'];
        }

        $params['select'] = $select;

        if ($filter) {
            $params['filter'] = $filter;
        }

        if ($start) {
            $params['order'] = $order;
        }

        if ($start) {
            $params['start'] = $start;
        }

        return $this->call('crm.contact.list', $params, ListResponse::class);
    }

    /**
     * @return FieldsResponse
     * @throws \Domatskiy\Bitrix24\Exception\ResponseException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @see https://dev.1c-bitrix.ru/rest_help/crm/cdeals/crm_deal_fields.php
     */
    public function fields():FieldsResponse
    {
        return $this->call('crm.deal.fields', [], FieldsResponse::class);
    }
}
