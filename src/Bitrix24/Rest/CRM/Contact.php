<?php

namespace Domatskiy\Bitrix24\Rest\CRM;

use Domatskiy\Bitrix24\Response\CRM\Lead\ListResponse;
use Domatskiy\Bitrix24\Rest\Method;

class Contact extends Method
{
    /**
     * @param array $select
     * @param array|null $filter
     * @param array|null $order
     * @param null $start
     * @return ListResponse
     * @throws \Domatskiy\Bitrix24\Exception\ResponseException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @see https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_list.php
     */
    public function list(array $select = [], ?array $filter = array(), ?array $order = null, $start = null):ListResponse
    {
        $params = [];

        if (empty($select)) {
            $select = ['ID', 'TITLE'];
        }

        $params['select'] = $select;

        if ($filter) {
            $params['filter'] = $filter;
        }

        if ($start) {
            $params['order'] = $order;
        }

        if ($start) {
            $params['start'] = $start;
        }

        return $this->call('crm.contact.list', $params, ListResponse::class);
    }
}
