<?php

namespace Domatskiy\Bitrix24\Rest;

use Domatskiy\Bitrix24\ApplicationInterface;
use Domatskiy\Bitrix24\Exception\ResponseException;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\NullLogger;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Method
 * @package Domatskiy\Bitrix24
 */
abstract class Method implements MethodInterface
{
    protected $app;

    protected $logger;

    public function __construct(ApplicationInterface $app)
    {
        $this->app = $app;
        $this->logger = $app->getLogger();

        if (!$this->logger) {
            $this->logger = new NullLogger();
        }
    }

    /**
     * @param $method
     * @param array $data
     * @param string $responseClass
     * @return Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws ResponseException
     * @throws \Exception
     */
    public function call($method, array $data, string $responseClass): Response
    {
        if (!class_exists(str_replace('[]', '', $responseClass))) {
            throw new \Exception(sprintf('class "%s" not exits', $responseClass));
        }

        try {
            $response = $this->app->request($method, $data);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $content = $response->getBody()->getContents();
            $this->logger->info('ClientException: '.$e->getMessage(), [
                'status' => $response->getStatusCode(),
                'content' => $content,
            ]);

            $ex = new ResponseException('ClientException', $response->getStatusCode());

            $data = json_decode($content, true);
            if ($data && isset($data['error'])) {
                $ex->setErrors([
                    $data['error'],
                ]);
            }

            if ($data && isset($data['error_description'])) {
                $ex->setErrorsDescriptions([
                    $data['error_description'],
                ]);
            }

            throw $ex;
        }

        $body = $response->getBody()->getContents();

        $data = json_decode($body);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $err = 'parse response body error: ';
            $err .= json_last_error().' '.json_last_error_msg().', data: '.var_export($body, true);
            $this->logger->error($err);
            throw new \Exception('not valid response');
        }

        $this->logger->debug('Denormalize response', [
            'responseClass' => $responseClass,
            'data' => $data,
        ]);

        /**
         * @var $rs Response
         */
        $rs = $this->getSerializer()->denormalize($data, $responseClass);
        $rs->setStatus($response->getStatusCode());

        return $rs;
    }

    protected function getSerializer():Serializer
    {
        // normalizers
        $normalizers = [
            new DateTimeNormalizer(),
            new ArrayDenormalizer(),
            new ObjectNormalizer(null, null, new PropertyAccessor(), new PhpDocExtractor())
        ];

        return new Serializer($normalizers);
    }
}
