<?php

namespace Domatskiy\Bitrix24\Rest;

use Domatskiy\Bitrix24\ApplicationInterface;

interface MethodInterface
{
    public function __construct(ApplicationInterface $app);

    public function call($method, array $data, string $responseClass):Response;
}
