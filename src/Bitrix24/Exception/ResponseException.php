<?php

namespace Domatskiy\Bitrix24\Exception;

class ResponseException extends \Exception
{
    /**
     * @var int
     */
    protected $code;

    /**
     * @var string[]
     */
    protected $errors = [];

    /**
     * @var string[]
     */
    protected $errorsDescriptions = [];

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string[] $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @return string[]
     */
    public function getErrorsDescriptions(): array
    {
        return $this->errorsDescriptions;
    }

    /**
     * @param string[] $errorsDescriptions
     */
    public function setErrorsDescriptions(array $errorsDescriptions): void
    {
        $this->errorsDescriptions = $errorsDescriptions;
    }
}
