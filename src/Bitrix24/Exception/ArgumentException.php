<?php

namespace Domatskiy\Bitrix24\Exception;

/**
 * Class ArgumentException
 * @package Domatskiy\Bitrix24\Exception
 */
class ArgumentException extends \Exception
{
    //
}
